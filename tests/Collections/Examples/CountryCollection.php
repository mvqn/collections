<?php
declare(strict_types=1);

namespace Tests\MVQN\Collections\Examples;

use MVQN\Collections\Collectible;
use MVQN\Collections\Collection;

use Tests\MVQN\Collections\Examples\Country;

/**
 * Class CountryCollection
 *
 * @package UCRM\REST\Endpoints\Collections
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
final class CountryCollection extends Collection
{
    /**
     * @param Collectible[]|null $elements
     * @throws \Exception
     */
    public function __construct(?array $elements = [])
    {
        parent::__construct(Country::class, $elements);
    }

    /**
     * @return Country[]
     */
    public function toArray(): array
    {
        return $this->elements();
    }
}
